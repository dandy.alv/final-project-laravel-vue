<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Mail\UserRegisteredMail;
use Mail;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
            'name' => 'required',
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt(request('password'))
        ]);

        //membuat array data sekaligus membuat property user dan memasukkan value nya
        $data['user'] = $user;

        event(new UserRegisteredEvent($user));


        return response()->json([
            'response_code' => '00',
            'response_message' => 'User Berhasil Daftar, silahkan cek email untuk melihat kode OTP',
            'data' => $data 
        ]);
    }
}
