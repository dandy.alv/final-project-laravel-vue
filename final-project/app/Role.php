<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Role extends Model
{
    use UseUuid;

    protected $fillable = ['name'];

    public function user(){
        return $this->hasMany('App\User', 'role_id');
    }
}
