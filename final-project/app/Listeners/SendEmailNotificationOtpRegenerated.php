<?php

namespace App\Listeners;

use App\Events\OtpRegeneratedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\RegenerateOtpCodeMail;

class SendEmailNotificationOtpRegenerated implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpRegeneratedEvent  $event
     * @return void
     */
    public function handle(OtpRegeneratedEvent $event)
    {
        Mail::to($event->user)->send(new RegenerateOtpCodeMail($event->user));
    }
}
