<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '3aaf4327-fefb-42ae-89ba-1d7d1e238516',
        ]);
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '751e0339-7c16-4787-8697-7a7e0e016219',
        ]);
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'role_id' => '3aaf4327-fefb-42ae-89ba-1d7d1e238516',
        ]);
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '3aaf4327-fefb-42ae-89ba-1d7d1e238516',
        ]);
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => '751e0339-7c16-4787-8697-7a7e0e016219',
        ]);
    }
}
