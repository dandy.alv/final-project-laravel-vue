<?php

use Illuminate\Database\Seeder;
use App\Campaign;

class CampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campaign::create([
            'title' => 'Galang Dana Covid 19',
            'description' => 'Assalamualaikum nama saya Rudi, dihimbau kepada semua warga supaya mendonasikan seadanya'
        ]);

        Campaign::create([
            'title' => 'Galang Dana Covid 18',
            'description' => 'Assalamualaikum nama saya Budiman, dihimbau kepada semua warga supaya mendonasikan seadanya'
        ]);

        Campaign::create([
            'title' => 'Galang Dana Covid 20',
            'description' => 'Assalamualaikum nama saya Adian, dihimbau kepada semua warga supaya mendonasikan seadanya'
        ]);
    }
}
